clear, clc, close all;

% ***************** Load data ********************
file1 = fopen ('./build/object_volume_xy.txt', 'r');
x_dimension = str2double(fgets(file1));
y_dimension = str2double(fgets(file1));
z_dimension = str2double(fgets(file1));
object_volume_xy = fscanf (file1, '%f');

file2 = fopen ('./build/scene_volume_xy.txt','r');
fgets(file2);
fgets(file2);
fgets(file2);
scene_volume_xy = fscanf (file2, '%f');

file3 = fopen ('./build/object_volume_z.txt', 'r');
fgets(file3);
fgets(file3);
fgets(file3);
object_volume_z = fscanf (file3, '%f');

file4 = fopen ('./build/scene_volume_z.txt','r');
fgets(file4);
fgets(file4);
fgets(file4);
scene_volume_z  = fscanf (file4, '%f');

object_xy = reshape(object_volume_xy, [y_dimension, x_dimension]);
scene_xy = reshape(scene_volume_xy, [y_dimension,x_dimension]);

%***************** Show  ***************

x = 1: z_dimension;

subplot(2,2,1)
imshow(object_xy', [])
title('object_xy');
subplot(2,2,2)
imshow(scene_xy', [])
subplot(2,2,3)
title('scene_xy');
plot(x, object_volume_z, 'g')
title('object_z');
subplot(2,2,4)
plot(x, scene_volume_z, 'r')
title('volume_z');

figure
subplot(1,2,1)
imshow(object_xy, [])
subplot(1,2,2)
imshow(scene_xy,[])

figure
subplot(2,1,1)
plot(x, object_volume_z, 'g', 'LineWidth',2)
subplot(2,1,2)
plot(x, scene_volume_z, 'r', 'LineWidth',2)

%----------- Thesis --------
figure, imshow(object_xy', [])
figure, imshow(scene_xy', [])

figure, plot(x, object_volume_z, 'g', 'LineWidth',2)
figure, plot(x, scene_volume_z, 'r', 'LineWidth',2)

