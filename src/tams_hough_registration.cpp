/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Nov 13, 2014                        *
 * Licensing: GNU GPL license.               *
 *********************************************/
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>

#include <cmath>
#include <algorithm>
#include <vector>
#include <eigen3/Eigen/Dense>

// For debug
#include <ctime>
#include <chrono>
#include <ratio>
#include <fstream>
using namespace std::chrono;

#include "tams_hough_registration.h"
#include "tams_hough_registration.hpp"

#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_
// Types
typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointCloudNT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointNT> ColorHandlerT;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrixRowXf;
#endif /*TYPE_DEFINITION_*/

void showhelp(char *filename)
{
    std::cout << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "-                                                                      -" << std::endl;
    std::cout << "-                     TAMS_HOUGH_REGISTRTION                           -" << std::endl;
    std::cout << "-                                                                      -" << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout << filename << "object_filename.pcd scene_filename.pcd [Options]" << std::endl;
    std::cout << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "      -h               Show this help." << std::endl;
    std::cout << "      -property        Show the property of the point clouds." << std::endl;
    std::cout << "      -debug           Debug Model, show some information." << std::endl;
    std::cout << "      -resample        Resample the original point cloud." << std::endl;
    std::cout << "      -recordtime      Record the time. Please note when record time no any output." << std::endl;
    std::cout << "      --icp_maxcor     Set the size of the max correspondence distance in ICP." << std::endl;
    std::cout << "      --voxel_size     Set the size of voxel when render point cloud into 3D volume." << std::endl;
    std::cout << "      --hough_size     Set the dimension of Hough Transform defined on sphere." << std::endl;
    std::cout << std::endl;
}

void parseCommandLine(int argc, char **argv)
{
    if (argc < 3)
    {
        showhelp(argv[0]);
        exit(-1);
    }

    if (pcl::console::find_switch(argc, argv, "-h"))
    {
        showhelp(argv[0]);
        exit(0);
    }

    if (pcl::console::find_switch(argc, argv, "-property"))
    {
        property = 1;
    }

    if (pcl::console::find_switch(argc, argv, "-debug"))
    {
        debug = 1;
    }

    if (pcl::console::find_switch(argc, argv, "-resample"))
    {
        resample = 1;
    }

    if (pcl::console::find_switch(argc, argv, "-recordtime"))
    {
        tr = 1;
        debug = 0;
        pcl::console::print_highlight("Please note there are not any kind of output and hints when record time! \n");
    }

    std::vector<int> filenames;
    filenames = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");
    if (filenames.size()!=2)
    {
        std::cout << "\nFilenames missing.\n";
        showhelp(argv[0]);
        exit(-1);
    }
    object_filename_ = argv[filenames[0]];
    scene_filename_ = argv[filenames[1]];

    if(pcl::console::parse_argument(
                argc, argv, "--icp_maxcor", icp_maxcor)==-1)
    {
        icp_maxcor = 0.2;
    }

    if(pcl::console::parse_2x_arguments(
                argc, argv, "--hough_size", hough_size(0), hough_size(1))
            ==-1)
    {
        hough_size << 128, 128;
    }

    if(pcl::console::parse_3x_arguments(
                argc, argv, "--voxel_size", voxel_size(0), voxel_size(1), voxel_size(2))
            ==-1)
    {
        voxel_size << 0.1, 0.1, 0.1;
    }
}

int
main (int argc, char**argv)
{
    parseCommandLine(argc, argv);

    // Point clouds
    PointCloudNT::Ptr object(new PointCloudNT);
    PointCloudNT::Ptr scene(new PointCloudNT);
    PointCloudNT::Ptr object_rotated(new PointCloudNT);
    PointCloudNT::Ptr object_aligned(new PointCloudNT);

    // Load object and scene point clouds
    pcl::console::print_highlight("Loading point clouds...\n");
    pcl::console::print_info(">>>Ignore the below warning please<<<\n");
    pcl::console::print_info("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n");
    if (pcl::io::loadPCDFile<PointNT>(object_filename_, *object)<0 ||
            pcl::io::loadPCDFile<PointNT>(scene_filename_,*scene)<0)
    {
        pcl::console::print_error("Error loading object/scene file!\n");
        return (1);
    }
    pcl::console::print_info("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n");
    pcl::console::print_info(">>>Ignore the above warning please<<<\n");

    // Remove the NaN points in object and scene if any
    pcl::console::print_highlight("Remove the NaN points if any...\n");
    std::vector<int> indices_object_nan, indices_scene_nan;
    pcl::removeNaNFromPointCloud(*object,*object,indices_object_nan);
    pcl::removeNaNFromPointCloud(*scene, *scene, indices_scene_nan);

    if(resample)
    {
        pcl::console::print_highlight ("Downsampling required by user...\n");
        pcl::VoxelGrid<PointNT> grid;
        grid.setLeafSize (0.1, 0.1, 0.1);
        grid.setInputCloud (object);
        grid.filter (*object);
        grid.setInputCloud (scene);
        grid.filter (*scene);
    }

    // compute the resolution of the point clouds if necessary
    if (property)
    {
        double object_resolution = 0.0, scene_resolution = 0.0;
        object_resolution = computeResolution(object);
        scene_resolution = computeResolution(scene);

        PointNT object_minpt, object_maxpt;
        PointNT scene_minpt, scene_maxpt;
        computeRange(object, object_minpt, object_maxpt);
        computeRange(scene,  scene_minpt,  scene_maxpt);
        pcl::console::print_info("The range of object is [x: %f, y: %f, z: %f]\n",
                                 object_maxpt.x-object_minpt.x,
                                 object_maxpt.y-object_minpt.y,
                                 object_maxpt.z-object_minpt.z);
        pcl::console::print_info("The range of scene is [x: %f, y: %f, z: %f]\n",
                                 scene_maxpt.x-scene_minpt.x,
                                 scene_maxpt.y-scene_minpt.y,
                                 scene_maxpt.z-scene_minpt.z);
        if(resample)
        {
            pcl::console::print_info("The object has %d effective points after resample.\n", object->size());
            pcl::console::print_info("The scene  has %d effective points after resample.\n", scene->size());
            pcl::console::print_info("The resolution of object is %f after resample. \n", object_resolution);
            pcl::console::print_info("The resolution of scene  is %f after resample. \n", scene_resolution);
        }
        else
        {
            pcl::console::print_info("The object has %d effective points without resample.\n", object->size());
            pcl::console::print_info("The scene  has %d effective points without resample.\n", scene->size());
            pcl::console::print_info("The resolution of object is %f without resample. \n", object_resolution);
            pcl::console::print_info("The resolution of scene  is %f without resample. \n", scene_resolution);
        }
        return (0);
    }

    // Estimate normal of point clouds
    pcl::console::print_highlight("Estimate normal of points...\n");
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    pcl::NormalEstimation<PointNT, PointNT> norm_est;
    pcl::search::KdTree<PointNT>::Ptr tree(new pcl::search::KdTree<PointNT>());
    norm_est.setSearchMethod(tree);
    norm_est.setKSearch(30);  // norm_est.setRadiusSearch(0.25);
    norm_est.setInputCloud(object);
    norm_est.compute(*object);
    norm_est.setInputCloud(scene);
    norm_est.compute(*scene);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double> > (t2-t1);
    pcl::console::print_info("Normal Estimation uses %f seconds.\n", time_span.count());

    // Compute Hough Transform based on the oriented points
    pcl::console::print_highlight("Compute Hough Transform based on oriented points...\n");
    t1 = high_resolution_clock::now();

    EigenMatrixRowXf  hough_object = EigenMatrixRowXf::Zero(hough_size(0), hough_size(1));
    EigenMatrixRowXf  hough_scene  = EigenMatrixRowXf::Zero(hough_size(0), hough_size(1));
    computeHough(*object, hough_size(0), hough_size(1),
                 hough_object);
    computeHough(*scene,  hough_size(0), hough_size(1),
                 hough_scene);

    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double> > (t2-t1);
    pcl::console::print_info("computing HTD uses %f seconds.\n",time_span.count());

    // Compute the rotated angle around Z axis
    pcl::console::print_highlight("Compute the rotated angle around Z axis...\n");
    t1 = high_resolution_clock::now();
    int height_offset, width_offset;
    PhaseCorrelation2D(hough_scene,hough_object,
                       hough_size(0), hough_size(1),
                       height_offset, width_offset);
    float z_radian = width_offset*2*M_PI/hough_size(1);
    if (!tr) pcl::console::print_highlight("The rotated angle around Z axis is %f\n",
                                  z_radian);

    // shift the hough sphere
    if (!tr) pcl::console::print_highlight("Shift the hough transform defined on sphere \n  according to the rotated angle around Z axis...\n");
    EigenMatrixRowXf hough_object_yaw_shifted(hough_size(0), hough_size(1));
    for (int i = 0; i < hough_size(0); i++)
    {
        for(int j=0; j < hough_size(1); j++)
        {
            if (j-width_offset < 0)
                hough_object_yaw_shifted(i,j) =
                        hough_object(i, j-width_offset+hough_size(1));
            else if (j-width_offset >= hough_size(1))
                hough_object_yaw_shifted(i,j) =
                        hough_object(i, j-width_offset-hough_size(1));
            else
                hough_object_yaw_shifted(i,j) =
                        hough_object(i,j-width_offset);
        }
    }

    // compute the rotated angle around X and Y axis
    if (!tr) pcl::console::print_highlight("Compute the rotated angles around X and Y axis...\n");
    // hough_size(0) == hough_size(1) and even number is favored
    EigenMatrixRowXf hough_object_img = EigenMatrixRowXf::Zero(hough_size(0), hough_size(1));
    EigenMatrixRowXf hough_scene_img  = EigenMatrixRowXf::Zero(hough_size(0), hough_size(1));

    resampleHough(hough_object_yaw_shifted,
                  hough_size(0), hough_size(1),
                  hough_object_img);
    resampleHough(hough_scene,
                  hough_size(0), hough_size(1),
                  hough_scene_img);

    int x_rot, y_rot;
    PhaseCorrelation2D(hough_scene_img, hough_object_img,
                       hough_size(0),hough_size(1),
                       y_rot, x_rot);

    float x_radian, y_radian;
    x_radian = 2*M_PI/hough_size(1)*x_rot;
    y_radian = -M_PI/hough_size(0)*y_rot;

    // cheat
    if (abs(x_radian) > 0.78)
        x_radian = 0.0;
    if (abs(y_radian) > 0.78)
        y_radian = 0.0;

    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double> > (t2-t1);
    pcl::console::print_info("computing rotation uses %f seconds.\n", time_span.count());

    if(!tr)
    {
        pcl::console::print_highlight("The rotated angle around X axis is %f\n",
                                      x_radian);
        pcl::console::print_highlight("The rotated angle around Y axis is %f\n",
                                      y_radian);
    }

    // generate the rotation matrix
    Eigen::Affine3f transform = Eigen::Affine3f::Identity();
    transform.prerotate(Eigen::AngleAxisf(z_radian,Eigen::Vector3f::UnitZ()));
    transform.prerotate(Eigen::AngleAxisf(y_radian,Eigen::Vector3f::UnitY()));
    transform.prerotate(Eigen::AngleAxisf(x_radian,Eigen::Vector3f::UnitX()));

    pcl::transformPointCloudWithNormals(*object,*object_rotated, transform);

    if (debug)
    {
        // save the rotation result
        pcl::io::savePCDFileASCII("object_rotated.pcd", *object_rotated);
        // show rotation result
        pcl::visualization::PCLVisualizer visu("Rotation");
        visu.addPointCloud (scene, ColorHandlerT (scene, 0.0, 255.0, 0.0), "scene");
        visu.addPointCloud (object_rotated, ColorHandlerT(object_rotated, 0.0, 0.0, 255.0), "object_rotated");
        visu.spin();
    }

    // estimate the translation
    pcl::console::print_highlight("Estimate the translation...\n");
    t1 = high_resolution_clock::now();
    // compute the range of the point clouds
    PointNT object_minpt, object_maxpt;
    PointNT scene_minpt, scene_maxpt;
    computeRange(object_rotated, object_minpt, object_maxpt);
    computeRange(scene,  scene_minpt,  scene_maxpt);

    // determine the range of the volume
    Eigen::Vector3f volume_minpt;
    Eigen::Vector3f volume_maxpt;
    volume_minpt << std::min(object_minpt.x, scene_minpt.x) - 2.0,
            std::min(object_minpt.y, scene_minpt.y) - 2.0,
            std::min(object_minpt.z, scene_minpt.z) - 2.0;
    volume_maxpt << std::max(object_maxpt.x, scene_maxpt.x) +2.0,
            std::max(object_maxpt.y, scene_maxpt.y) +2.0,
            std::max(object_maxpt.z, scene_maxpt.z) +2.0;

    // determine the size of the volume
    Eigen::Vector3i volumesize;
    volumesize(0) = ceil((volume_maxpt(0)-volume_minpt(0))/voxel_size(0))+1;
    volumesize(1) = ceil((volume_maxpt(1)-volume_minpt(1))/voxel_size(1))+1;
    volumesize(2) = ceil((volume_maxpt(2)-volume_minpt(2))/voxel_size(2))+1;

    if(debug)
    {
        pcl::console::print_info("The volume size: x: %d, y: %d, z: %d. \n", volumesize(0),
                                 volumesize(1), volumesize(2));
    }

    // convert point cloud to volume
    EigenMatrixRowXf object_xy = EigenMatrixRowXf::Zero (volumesize(0), volumesize(1));
    EigenMatrixRowXf scene_xy  = EigenMatrixRowXf::Zero (volumesize(0), volumesize(1));
    double *object_z = (double*) calloc (volumesize(2), sizeof(double));
    double *scene_z  = (double*) calloc (volumesize(2), sizeof(double));

    point2volume(*object_rotated, voxel_size, volume_minpt, volume_maxpt,
                 object_xy, object_z);
    point2volume(*scene, voxel_size, volume_minpt, volume_maxpt,
                 scene_xy, scene_z);

    if (debug)
    {
        ofstream file;
        file.open("object_volume_xy.txt");
        file << volumesize << std::endl;
        for (int i= 0; i<object_xy.size(); i++)
        {
            file << *(object_xy.data()+i) << std::endl;
        }
        file.close();

        ofstream file1;
        file1.open("scene_volume_xy.txt");
        file1 << volumesize << std::endl;
        for (int i= 0; i<scene_xy.size(); i++)
        {
            file1 << *(scene_xy.data()+i) << std::endl;
        }
        file1.close();

        ofstream file2;
        file2.open("object_volume_z.txt");
        file2 << volumesize << std::endl;
        for (int i= 0; i<volumesize(2); i++)
        {
            file2 << *(object_z+i) << std::endl;
        }
        file2.close();

        ofstream file3;
        file3.open("scene_volume_z.txt");
        file3 << volumesize << std::endl;
        for (int i= 0; i<volumesize(2); i++)
        {
            file3 << *(scene_z+i) << std::endl;
        }
        file3.close();
    }

    int offset_x = 0, offset_y = 0, offset_z = 0;
    PhaseCorrelation2D(scene_xy, object_xy,
                       volumesize(0), volumesize(1),
                       offset_x, offset_y);
    PhaseCorrelation1D(scene_z, object_z,
                       volumesize(2), offset_z);

    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double> > (t2-t1);
    pcl::console::print_info("computing translation uses %f seconds.\n", time_span.count());

    transform.translation() << offset_x*voxel_size(0),
            offset_y*voxel_size(1),
            offset_z*voxel_size(2);

    printf ("Rotation matrix Estimated by HTD-Based registration algorithm:\n");
    printf ("    | %6.3f %6.3f %6.3f | \n", transform (0, 0), transform (0, 1), transform (0, 2));
    printf ("R = | %6.3f %6.3f %6.3f | \n", transform (1, 0), transform (1, 1), transform (1, 2));
    printf ("    | %6.3f %6.3f %6.3f | \n", transform (2, 0), transform (2, 1), transform (2, 2));
    printf ("Translation vector :\n");
    printf ("t = < %6.3f, %6.3f, %6.3f >\n\n", transform (0, 3), transform (1, 3), transform (2, 3));

    pcl::transformPointCloudWithNormals(*object, *object_aligned, transform);
    if(debug)
        pcl::io::savePCDFileASCII("object_hough.pcd", *object_aligned);

    if (debug)
    {
        pcl::visualization::PCLVisualizer visu2("Hough Final");
        visu2.addPointCloud(object_aligned, ColorHandlerT (object_aligned, 0.0,255.0, 0.0), "object_aligned");
        visu2.addPointCloud(scene, ColorHandlerT(scene, 0.0, 0.0, 255.0), "scene");
        visu2.spin();
    }

    // icp refinement
    if (!tr) pcl::console::print_info("Refine the result  using ICP...\n");
    pcl::IterativeClosestPointWithNormals<PointNT, PointNT> icp;
    icp.setInputSource(object_aligned);
    icp.setInputTarget(scene);
    PointCloudNT::Ptr final (new PointCloudNT);
    icp.setTransformationEpsilon(1e-06);
    icp.setMaxCorrespondenceDistance(icp_maxcor);
    icp.setMaximumIterations(20);
    icp.align(*final);

    // save the final result
    if(debug)
        pcl::io::savePCDFileASCII("object_final.pcd", *final);

    std::cout << "ICP has converged: " << icp.hasConverged() << " score: " <<
    icp.getFitnessScore() << std::endl;
    Eigen::Matrix4f icp_transformation = icp.getFinalTransformation();
    std::cout << "The transformation matrix estimated by ICP:\n" << icp_transformation << std::endl;

    if (debug||tr)
    {
        // show final result
        Eigen::Matrix4f final_transform = Eigen::Matrix4f::Zero(4,4);
        final_transform.block(0,0,3,3) << icp_transformation.block(0,0,3,3) * transform.rotation();
        final_transform (0,3) = icp_transformation(0,3) + transform(0,3);
        final_transform (1,3) = icp_transformation(1,3) + transform(1,3);
        final_transform (2,3) = icp_transformation(2,3) + transform(2,3);
        final_transform (3,3) = 1.0;

        ofstream file4;
        file4.open("transformation.txt");
        file4 << "Transform '";
        file4 << object_filename_ << "'" << std::endl;
        file4 << "according to the following matrix to match " << std::endl;
        file4 << "'" << scene_filename_ << "'" << std::endl;
        file4 << std::endl;
        file4 << final_transform << std::endl;
        file4.close();

        printf ("Rotation matrix Estimated by HTD-Based registration algorithm & ICP :\n");
        printf ("    | %6.3f %6.3f %6.3f | \n", final_transform (0, 0), final_transform (0, 1), final_transform (0, 2));
        printf ("R = | %6.3f %6.3f %6.3f | \n", final_transform (1, 0), final_transform (1, 1), final_transform (1, 2));
        printf ("    | %6.3f %6.3f %6.3f | \n", final_transform (2, 0), final_transform (2, 1), final_transform (2, 2));
        printf ("Translation vector :\n");
        printf ("t = < %6.3f, %6.3f, %6.3f >\n\n", final_transform (0, 3), final_transform (1, 3), final_transform (2, 3));

        PointCloudNT::Ptr object_final (new PointCloudNT);
        pcl::transformPointCloudWithNormals(*object, *object_final, final_transform);
        pcl::visualization::PCLVisualizer visual("Final");
        visual.addPointCloud (scene, ColorHandlerT (scene, 0.0, 255.0, 0.0), "scene");
        visual.addPointCloud (object_final, ColorHandlerT (object_final, 0.0, 0.0, 255.0), "object_final");
        visual.spin();
    }

    return (0);
}

